const functions = require("firebase-functions");
const Meta = require("html-metadata-parser");

function strBetween(str, str1, str2) {
  return str.substring(str.indexOf(str1) + str1.length, str.indexOf(str2));
}

exports.tiktok = functions.https.onRequest((req, res) => {
  const id = req.query.id;
  const username = id.indexOf("@") == 0 ? id : "@" + id;

  var link = "https://www.tiktok.com/" + username;
  Meta.parser(link, function (err, result) {
    const { og } = result;
    const { title, description, images } = og;

    const name = title.substring(0, title.indexOf(" on TikTok"));
    const followers = strBetween(
      description,
      "(" + username + ")",
      "Followers"
    );
    const following = strBetween(description, "Followers, ", " Following");
    const likes = strBetween(description, "Following, ", " Likes");

    const avatar = images[0].url;

    const user = { username, avatar, name, following, followers, likes };
    res.send(user);
  }).catch(res.send);
});
